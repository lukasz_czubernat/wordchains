package chain.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
public class DictionaryTest {

  @Mock
  FileLoader fileLoader;

  Dictionary dictionary;

  @Before
  public void init() {
    doReturn(simpleWordList()).when(fileLoader).loadFileLines(any());
    dictionary = new Dictionary(fileLoader);
//    when(fileLoader.loadFileLines(any())).thenReturn(simpleWordList());
  }

  @Test
  public void isWord() throws Exception {
    //given
    String wordToCheck = "cat";

    // when - then
    Assert.assertTrue(dictionary.isWord(wordToCheck));
  }

  @Test
  public void isNotWord() throws Exception {
    //given
    String wordToCheck = "horse";

    // when - then
    Assert.assertFalse(dictionary.isWord(wordToCheck));
  }

  @Test
  public void isWordWithNull() throws Exception {
    //given
    String wordToCheck = null;

    // when - then
    Assert.assertFalse(dictionary.isWord(wordToCheck));
  }

  @Test
  public void checkIfLengthOfWordsIsSame() throws Exception {
    //given
    String firstWord = "word";
    String secondWord = "door";

    // when - then
    Assert.assertTrue(dictionary.checkIfLengthOfWordsIsSame(firstWord, secondWord));
  }

  @Test
  public void checkIfLengthOfWordsIsNotSame() throws Exception {
    //given
    String firstWord = "word";
    String secondWord = "anvil";

    // when - then
    Assert.assertFalse(dictionary.checkIfLengthOfWordsIsSame(firstWord, secondWord));
  }

  @Test
  public void checkIfLengthOfNullWordsIsSame() throws Exception {
    //given
    String firstWord = null;
    String secondWord = null;

    // when - then
    Assert.assertFalse(dictionary.checkIfLengthOfWordsIsSame(firstWord, secondWord));
  }

  @Test
  public void getWordsOfLength() throws Exception {
    //given
    int length = 4;

    // when - then
    Assert.assertThat(dictionary.getWordsOfLength(length), hasSize(2));
  }

  @Test
  public void areWordsDiffByOneChar() throws Exception {
    //given
    String firstWord = "word";
    String secondWord = "ward";

    // when - then
    Assert.assertTrue(dictionary.areWordsDiffByOneChar(firstWord, secondWord));
  }

  @Test
  public void areWordsNotDiffByOneChar() throws Exception {
    //given
    String firstWord = "word";
    String secondWord = "dart";

    // when - then
    Assert.assertFalse(dictionary.areWordsDiffByOneChar(firstWord, secondWord));
  }

  @Test
  public void areWordsDiffByOneCharWithNull() throws Exception {
    //given
    String firstWord = "word";
    String secondWord = null;

    // when - then
    Assert.assertFalse(dictionary.areWordsDiffByOneChar(firstWord, secondWord));
  }

  @Test
  public void getWordsWithOneCharDiff() throws Exception {
    //given
    String word = "cat";

    // when - then
    Assert.assertThat(dictionary.getWordsWithOneCharDiff(word), hasSize(2));
  }

  @Test
  public void getWordsWithOneCharDiffWithNull() throws Exception {
    //given
    String word = null;

    // when - then
    Assert.assertThat(dictionary.getWordsWithOneCharDiff(word), hasSize(0));
  }

  private List<String> simpleWordList() {
    return new LinkedList<String>() {{
      add("cat");
      add("car");
      add("nat");
      add("dog");
      add("door");
      add("anvil");
      add("test");
    }};
  }
}