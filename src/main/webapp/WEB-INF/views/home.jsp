<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1">
    <title>Welcome page</title>
</head>
<body>
<c:url var="search" value="/search"/>
<form action="${search}" method="post">
    <div>
        <label>Start word</label>
        <input type="text" id="startWord" name="startWord" placeholder="Enter starting word" required>
    </div>
    <div>
        <label>End word</label>
        <input type="text" id="endWord" name="endWord" placeholder="Enter ending word" required>
    </div>

    <div>
        <input type="submit" value="Find word chain">
    </div>
</form>

</body>
</html>