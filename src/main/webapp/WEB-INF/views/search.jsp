<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="ISO-8859-1">
    <title>Result page</title>
</head>
<body>
<c:forEach items="${wordChain}" var="word">
    <span><c:out value="${word}"></c:out> </br></span>
</c:forEach>

<br/>
<div>
    <button type="button" name="back" onclick="history.back()">Back</button>
</div>
</body>
</html>