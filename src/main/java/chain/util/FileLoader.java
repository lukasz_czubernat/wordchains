package chain.util;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FileLoader {

  public List<String> loadFileLines(Path path) {
    try {
      return Files.lines(path).collect(Collectors.toList());
    } catch (IOException e) {
      e.getStackTrace();
    }
    return Collections.EMPTY_LIST;
  }
}
