package chain.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNoneBlank;

@Component
public class Dictionary {

  private FileLoader fileLoader;

  private static final String dictionaryName = "wordlist.txt";
  private List<String> wordList;

  @Autowired
  public Dictionary(FileLoader fileLoader) {
    this.wordList = fileLoader.loadFileLines(Paths.get(getUri()));
  }

  private URI getUri() {
    try {
      return ClassLoader.getSystemResource(dictionaryName).toURI();
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean isWord(String word) {
    return wordList.contains(isNoneBlank(word) ? word : EMPTY);
  }

  public boolean checkIfLengthOfWordsIsSame(String firstWord, String secondWord) {
    if (isBlank(firstWord) || isBlank(secondWord)) {
      return false;
    }
    return firstWord.length() == secondWord.length();
  }

  public List<String> getWordsOfLength(int length) {
    return wordList.stream()
        .filter(word -> word.length() == length)
        .collect(Collectors.toList());
  }

  public boolean areWordsDiffByOneChar(String firstWord, String secondWord) {
    if (isBlank(firstWord) || isBlank(secondWord)) {
      return false;
    }
    int diff = 0;
    for (int i = 0; i < firstWord.length(); i++) {
      if (firstWord.charAt(i) != secondWord.charAt(i)) {
        diff++;
      }
    }
    return diff == 1;
  }

  public List<String> getWordsWithOneCharDiff(String word) {
    if (isBlank(word)) {
      return new LinkedList<>();
    }
    return getWordsOfLength(word.length()).stream()
        .filter(w -> checkIfLengthOfWordsIsSame(word, w))
        .filter(w-> areWordsDiffByOneChar(word, w))
        .collect(Collectors.toList());
  }
}

