package chain.controller;

import chain.service.ChainResolverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ChainController {

  private ChainResolverService chainResolverService;

  @Autowired
  public ChainController(ChainResolverService chainResolverService) {
    this.chainResolverService = chainResolverService;
  }

  @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
  public String home() {
    return "home";
  }

  @RequestMapping(value = "/search", method = RequestMethod.POST)
  public String search(@RequestParam(value = "startWord") String start, @RequestParam(value = "endWord") String end, ModelMap modelMap) {
    List<String> resolve = chainResolverService.resolve(start, end);
    modelMap.addAttribute("wordChain", resolve);
    return "search";
  }

}
