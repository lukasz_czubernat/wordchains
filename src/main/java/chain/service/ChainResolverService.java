package chain.service;

import chain.util.Dictionary;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChainResolverService {

  private Dictionary dictionary;

  private Queue<String> possibleWords = new ArrayDeque<>();

  @Autowired
  public ChainResolverService(Dictionary dictionary) {
    this.dictionary = dictionary;
  }

  public List<String> resolve(String start, String end) {
    List<String> result = new LinkedList<>();
    if (dictionary.isWord(start) && dictionary.isWord(end) && dictionary.checkIfLengthOfWordsIsSame(start, end)) {
      if (start.equalsIgnoreCase(end)) {
        return new LinkedList<String>() {{
          add(end);
        }};
      }
      possibleWords.add(start);
      result = processWords(start, end);
    }
    possibleWords.clear();
    return CollectionUtils.isNotEmpty(result) ? result : new LinkedList<String>(){{add("No results for give words");}};
  }

  private List<String> processWords(String start, String end) {
    Map<String, String> checkedWords = new HashMap<>();
    String possibleWord;
    while (!possibleWords.isEmpty() && !(possibleWord = possibleWords.poll()).equalsIgnoreCase(end)) {
      final String copyOfPossibleWord = possibleWord;
      dictionary.getWordsWithOneCharDiff(possibleWord).stream().forEach(nextWord -> {
        if (!checkedWords.containsKey(nextWord)) {
          checkedWords.put(nextWord, copyOfPossibleWord);
          possibleWords.add(nextWord);
        }
        if (end.equals(copyOfPossibleWord)) {
          return;
        }
      });
    }
    return createWordChain(checkedWords, start, end);
  }

  private List<String> createWordChain(Map<String, String> checkedWords, String start, String end) {
    if (!checkedWords.containsKey(end)) {
      return null;
    }
    List<String> result = new LinkedList<>();
    String word = end;
    while (!word.equals(start)) {
      result.add(word);
      word = checkedWords.get(word);
    }
    result.add(start);
    Collections.reverse(result);
    return result;
  }


}
